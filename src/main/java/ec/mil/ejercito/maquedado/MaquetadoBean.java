/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.mil.ejercito.maquedado;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author User
 */
@Stateful
@ViewScoped
@Named
public class MaquetadoBean {

    @Inject
    protected UtilBean utilBean;

    private String seleccionado = "";

    private List<UsuariosDTO> listaUsuarios = new ArrayList<UsuariosDTO>();

    @PostConstruct
    public void init() {
        for (int i = 0; i < 15; i++) {
            listaUsuarios.add(new UsuariosDTO("Cedula" + i, "Nombre del usuaro " + i, "Observaciones del usuario " + i));
            System.out.println("---------- Agrega " + listaUsuarios.size());
        }
    }

    public String getSeleccionado() {
        return seleccionado;
    }

    public void setSeleccionado(String seleccionado) {
        this.seleccionado = seleccionado;
    }

    public List<UsuariosDTO> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(List<UsuariosDTO> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public void guardar() {
        utilBean.mostrarMensaje(2, "Correcto:", "Inormacion guardada con exito");
    }
}
