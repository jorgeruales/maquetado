package ec.mil.ejercito.maquedado;

import java.io.Serializable;
import java.util.Date;
import javax.ejb.Stateful;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author jruales En esta clase se agregaran todos los metodos genericos y
 * reutilizables, como validadores formateadores y mensajes
 */
@Stateful
@SessionScoped
@Named
public class UtilBean implements Serializable {

  

   
    public UtilBean() {

    }

    /**
     * @param severidad Indica severidad = 0 error, 1 dvertencia, 2 correcto
     * @param titulo Tittle of message
     * @param detalle Detail or content of message
     */
    public void mostrarMensaje( int severidad, String titulo, String detalle) {
        
        if (severidad == 0) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, titulo, detalle));
        }

        if (severidad == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, titulo, detalle));
        }

        if (severidad == 2) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, titulo, detalle));
        }

    }

    public Date getMomentoActual() {
        return new Date();
    }

    public Date getFechaDiaria() {
        Date hoy = new Date();
        hoy.setHours(0);
        hoy.setSeconds(0);
        hoy.setMinutes(0);
        return hoy;
    }

}
